# Weird Algorithm

Description: [https://cses.fi/problemset/task/1068](https://cses.fi/problemset/task/1068)

There are two implementations:

 - one that respects the constraints = `WeirdAlgorithm.java`
 - one that goes beyond the constraints' upper-bound = `WeirdAlgorithm2.java`

## Example run

```
$ git clone https://radman13666@bitbucket.org/radman13666/weird-algorithm.git
$ cd weird-algorithm
$ javac WeirdAlgorithm.java
$ # Example with wrong input
$ java WeirdAlgorithm -1
$ java WeirdAlgorithm x
$ # Example with small input
$ java WeirdAlgorithm 3
$ # Example with large input
$ java WeirdAlgorithm 99999999999999999
$ javac WeirdAlgorithm2.java
$ # Example with wrong input
$ java WeirdAlgorithm2 -1
$ java WeirdAlgorithm2 x
$ # Example with small input
$ java WeirdAlgorithm2 3
$ # Example with large input
$ java WeirdAlgorithm2 99999999999999999
```

More interesting facts related to this problem can be found in [this video](https://youtu.be/094y1Z2wpJg).
