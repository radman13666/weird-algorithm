// Project Description: https://cses.fi/problemset/task/1068

public class WeirdAlgorithm {
    private static int weirdAlgorithm(int n) {
        return (n % 2 == 0) ? n / 2 : (n * 3) + 1;
    }
    
    private static int getNumber(String n) {
        int number;
        try {
            number = Integer.parseInt(n);
            if (number <= 0 || number > 1_000_000) number = 1;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            number = 1;
        }
        return number;
    }

    public static void main(String[] args) {
        if (args.length != 1) return;

        int n = getNumber(args[0]);
        System.out.printf("%d ", n);

        do {
            n = weirdAlgorithm(n);
            System.out.printf("%d ", n);
        } while (n > 1);

        System.out.println();
    }
}
