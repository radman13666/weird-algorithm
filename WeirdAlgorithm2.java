// Project Description: https://cses.fi/problemset/task/1068

import java.math.BigInteger;

public class WeirdAlgorithm2 {
    private static final BigInteger TWO = new BigInteger("2");

    private static BigInteger weirdAlgorithm(BigInteger n) {
        try {
            if (n.remainder(TWO).compareTo(BigInteger.ZERO) == 0) { // if n % 2 == 0 (i.e. if even)
                return n.divide(TWO); // n / 2
            } else {
                return n.multiply(new BigInteger("3")).add(BigInteger.ONE); // (n * 3) + 1
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return BigInteger.ONE;
        }
    }
    
    private static BigInteger getNumber(String n) {
        BigInteger number;
        try {
            number = new BigInteger(n);
            if (number.compareTo(BigInteger.ZERO) <= 0) { // n <= 0
                number = BigInteger.ONE;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            number = BigInteger.ONE;
        }
        return number;
    }

    public static void main(String[] args) {
        if (args.length != 1) return;

        BigInteger n = getNumber(args[0]);
        System.out.printf("%s ", n);

        do {
            n = weirdAlgorithm(n);
            System.out.printf("%s ", n);
        } while (n.compareTo(BigInteger.ONE) > 0); // n > 1

        System.out.println();
    }
}
